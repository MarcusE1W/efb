;;; Personal configuration -*- lexical-binding: t -*-

;; Save the contents of this file under ~/.emacs.d/init.el
;; Do not forget to use Emacs' built-in help system:
;; Use C-h C-h to get an overview of all help commands.  All you
;; need to know about Emacs (what commands exist, what functions do,
;; what variables specify), the help system can provide.

;; ==========
;; = Themes =
;; ==========


;; Load a custom theme, choos only one
;; (load-theme 'dichromacy t)
;; (load-theme 'deeper-blue t)
;; (load-theme 'adwaita t)
(load-theme 'leuven t)
;; (load-theme 'light-blue t)
;; (load-theme 'manoj-dark t)
;; (load-theme 'misterioso t)
;; (load-theme 'modus-operandi t)  ;; light
;; (load-theme 'modus-vivendi t)  ;; dark
;; (load-theme 'tango-dark t)
;; (load-theme 'tango t)
;; (load-theme 'tsdh-dark t)
;; (load-theme 'wheatgrass t)
;; (load-theme 'whiteboard t)
;; (load-theme 'wombat t)




;; ====================
;; = Visual Settings =
;; ====================

;; Highlight trailing whitespace at the end of lines:
;; (setq-default show-trailing-whitespace t)

;; Set default font face and size (Linux)
(set-face-attribute 'default nil :font "LiberationMono" :height 140)

;; Disable the menu bar
(menu-bar-mode 1)
;; TODO, why does it show up sometimes anyway ?

;; Disable the tool bar
(when (display-graphic-p)
  (tool-bar-mode 0)
  (scroll-bar-mode 0))

;; Disable splash/start screen
;; (setq inhibit-startup-screen t)

;; Enable line numbering in `prog-mode'; + column numbers in modal line
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(column-number-mode)

;; Show the end of buffer with a special glyph in the left fringe
(setq-default indicate-empty-lines t)

;; == Tab settings ==

;; For very clear explaination of theses settings see here:
;; https://github.com/susam/emfy#indentation

;; Use spaces, not tabs, for indentation; turn tab mode off
(setq-default indent-tabs-mode 0)

;; Display the distance between two tab stops as whitespace that is as wide as 4 characters:
(setq-default tab-width 4)


;; == Parenthesis {([ ])}  (paren) ==

;; no delay to show matching parens; this lne needs to be first
(setq show-paren-delay 0)

;; Highlight matching parens
(show-paren-mode)

;; TODO set word-wrap in text modes

;; ====================
;; = General Settings =
;; ====================

;; Miscellaneous options
(setq-default major-mode
              (lambda () ; guess major mode from file name
                (unless buffer-file-name
                  (let ((buffer-file-name (buffer-name)))
                    (set-auto-mode)))))


(setq window-resize-pixelwise t)
(setq frame-resize-pixelwise t)


;; Store automatic customisation options elsewhere
(setq custom-file (locate-user-emacs-file "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; Always add a newline automatically at the end of a file while saving
(setq-default require-final-newline t)

;; Write customizations to a separate file instead of this file.
;; (setq custom-file (concat user-emacs-directory "custom.el"))   ;; <- needed ?

;; Consider a period followed by a single space to be end of sentence.
(setq sentence-end-double-space nil)

;; keyboard scroll one line at a time, 
(setq scroll-step 1)
;; (pixel-scroll-mode 1)


;; ==============
;; == Packages ==
;; ==============

;; a good explaination about package installation is here:
;; https://github.com/susam/emfy#install-packages

;; Enable installation of packages from MELPA.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install packages.
(dolist (package '(
				   use-package
				   markdown-mode
				   company
				   avy
				   consult
				   vertico
				   orderless
				   marginalia
				   which-key
				   expand-region
				   ))
  (unless (package-installed-p package)
    (package-install package)))


;; ====================
;; == Package Config ==
;; ====================

;; Expaination of the use-package decalarative configuration system
;; https://www.masteringemacs.org/article/spotlight-use-package-a-declarative-configuration-tool

;; ====================
;; == Use Package ==
;; ====================
 
(eval-when-compile
  (require 'use-package))

;; ==============
;; == Flyspell ==
;; ==============

(use-package flyspell
  :init
  (progn
    (flyspell-mode 1))
  :config
  (progn 
    (setq ispell-program-name "aspell")
    (setq ispell-list-command "--list") ;; run flyspell with aspell, not ispell
    ))

;; ====================
;; == Expand regions ==
;; ====================

(use-package expand-region
  :bind ("C-=" . er/expand-region))

;; ===============
;; == Which-key ==
;; ===============

;; (require 'which-key)
;; (which-key-mode)

(use-package which-key
 :init
  (which-key-mode)
  )


;; =============
;; == Avy  ==
;; =============

(use-package avy
  :bind ( ("C-c z" . avy-goto-word-1) ;; original code ?
		  ("C-c j" . avy-goto-word-1) ;; EfB j for jum
		  ))


;; =============
;; == Company ==
;; =============

;; (add-hook 'prog-mode-hook #'company-mode) 
(use-package company
  :hook (prog-mode . company-mode)) ;; Enable Company by default in programming buffers 


;; ================
;; == Org-indent ==
;; ================

(use-package org-indent
  :hook (org-mode . org-indent-mode)
  :config (setq org-indent-indentation-per-level 4)
		  (setq org-indent-mode-turns-on-hiding-stars nil))

;; TODO find better place
;;(setg org-support-shift-select t) => error message


;; =============
;; == fido  ==
;; =============

;; (use-package icomplete
;;   :bind ( ("C-p" . execute-extended-command) ;; EfB
;; 		  ("C-f" . isearch-forward) ;; EfB
;; 		  ("C-b" . switch-to-buffer) ;; EfB
;; 		  )
;;   :init
;;   (fido-vertical-mode))
;; TODO buffer-line
;; TODO recent-file
;; TODO bookmark



;; =============
;; == Vertico  ==
;; =============

(use-package vertico
  :bind ( "C-p" . execute-extended-command ) ;; EfB

  :init
  (vertico-mode)

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  ;; (setq vertico-cycle t)
  )


;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  ;; (defun crm-indicator (args)
  ;;   (cons (format "[CRM%s] %s"
  ;;                 (replace-regexp-in-string
  ;;                  "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
  ;;                  crm-separator)
  ;;                 (car args))
  ;;         (cdr args)))
  ;; (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; ;; Do not allow the cursor in the minibuffer prompt
  ;; (setq minibuffer-prompt-properties
  ;;       '(read-only t cursor-intangible t face minibuffer-prompt))
  ;; (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
        #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

;; Configure directory extension.
(use-package vertico-directory
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))


;; =================
;; == Orderless  ==
;; =================

;; Optionally use the `orderless' completion style.
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))


;; =============
;; == Consult  ==
;; =============

;; Example configuration for Consult
(use-package consult
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ;; ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ;; ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-b" . consult-buffer)                ;; EfB orig. switch-to-buffer
         ("C-c r" . consult-recent-file)                ;; EfB orig. recentf-list
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ;; ("M-#" . consult-register-load)
         ;; ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ;; ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("<help> a" . consult-apropos)            ;; orig. apropos-command
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ;; ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ;; ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g v" . consult-yank-from-kill-ring)   ;; EfB

         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ;; ("M-s G" . consult-git-grep)
         ;; ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("C-f" . consult-line) ;; EfB
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key (kbd "M-."))
  ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key (kbd "M-.")
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;; There are multiple reasonable alternatives to chose from.
  ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
  ;;;; 2. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
  ;;;; 3. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
  ;;;; 4. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
)


;; =================
;; == Marginalia  ==
;; =================

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  ;; Either bind `marginalia-cycle' globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))


;; ===================
;; == Modula 3 mode ==
;; ===================

;; Path to cm3 modula 3 mode
(add-to-list 'load-path "/home/mmw/.local/cm3-git/m3-tools/gnuemacs/src")

(autoload 'modula-3-mode "modula3")
        (setq auto-mode-alist 
             (append '(("\\.ig$" . modula-3-mode)
                       ("\\.mg$" . modula-3-mode)
                       ("\\.i3$" . modula-3-mode)
                       ("\\.m3$" . modula-3-mode))
                     auto-mode-alist))

(setq completion-ignored-extensions
      (append '(".mo" ".mx" ".mc" ".io" ".ix") completion-ignored-extensions))

;; ==================================================
;; = History, Backup, Recent/files/buffers/commands =
;; ==================================================

(save-place-mode t)

;; (savehist-mode t) ;; part of vertigo 
;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

(recentf-mode t)

;; ### have a larger list for recent files ###
(setq recentf-max-menu-items 50)
(setq recentf-max-saved-items 50)

; Write auto-saves and backups to separate directory.
(make-directory "~/.emacs.d/auto-save/" t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save/" t)))
(setq backup-directory-alist '(("." . "~/.emacs.d/backup/")))

;; Do not move the current file while creating backup. This keeps symlinks
(setq backup-by-copying t)

;; Disable lockfiles.
(setq create-lockfiles nil)


;; =========================
;; == Emacs for beginner  ==
;; =========================


;; allow text selection with shift and arrow keys
(global-unset-key (vector (list 'shift 'left)))
(global-unset-key (vector (list 'shift 'right)))
(global-unset-key (vector (list 'shift 'up)))
(global-unset-key (vector (list 'shift 'down)))

;; == duplicate line ==
;; new function to duplicate a line
;; docu for custom commands here:
;; https://github.com/susam/emfy#custom-command-and-key-sequence
(defun duplicate-line()
  "duplicate the current line"
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
  )

;; == set comment char ==
;; Set line-comment character(s) for current mode
(defun setcomchar (cc)
  "Sets the default comment characted in the current mode"
  (interactive "sCharacter::")
  (setq comment-start cc)
)

;; ===================
;; = EfB keybindings =
;; ===================

(global-set-key (kbd "C-S-d") 'duplicate-line) ;; EfB duplicate line
(global-set-key (kbd "C-S-k") 'kill-whole-line) ;; EfB delete whole line
(global-set-key (kbd "C-s") 'save-buffer) ;; EfB safe
(global-set-key (kbd "M-x") 'kill-region) ;; EfB cut
(global-set-key (kbd "M-c") 'kill-ring-save) ;; EfB copy
(global-set-key (kbd "M-v") 'yank) ;; EfB paste
(global-set-key (kbd "C-v") 'yank) ;; EfB paste (for compfort)

(global-set-key (kbd "C-x C-u") 'undo) ;; EfB same as C-x u to avoid confusion
(global-set-key (kbd "C-z") 'undo) ;; EfB undo
(global-set-key (kbd "C-r") 'query-replace) ;; EfB replace

